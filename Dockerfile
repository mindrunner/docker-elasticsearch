FROM elasticsearch:7.12.1

# Make sure pipes are considered to detemine success, see: https://github.com/hadolint/hadolint/wiki/DL4006
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN elasticsearch-plugin install -b ingest-attachment
